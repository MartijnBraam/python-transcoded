import argparse
import configparser
import os

from transcoded.job import TranscodingJob
from transcoded.__main__ import parse_profiles

parser = argparse.ArgumentParser()
parser.add_argument('inputfile')
args = parser.parse_args()
inputfile = args.inputfile

config = configparser.ConfigParser()
config.read('tests/test-config.ini')
profiles = parse_profiles(config)


def test_job(profile):
    job = TranscodingJob()
    job.sources.append(os.path.abspath('tests/inputs/{}'.format(inputfile)))
    job.destination = os.path.abspath('/tmp/{}.transcoded'.format(inputfile))
    job.profile = profile
    job.user = 'test'

    job.run()


for profile in profiles:
    test_job(profiles[profile])
